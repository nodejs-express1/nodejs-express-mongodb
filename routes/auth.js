const router  = require("express").Router(); // api를 router를 이용하여 작성한다. 
const User = require('../model/user'); // 정의한 몽고디비 스키마 import, 위치에 주의 한다. 


/**
 * 계정 등록하기
 */
router.post('/register', async (req, res) => {
    console.log("register");
    const user = new User({ // 스키마에 값 바인딩 
        name : req.body.name,
        email : req.body.email,
        password : req.body.password
    });
    console.log("name : "+ user.name);
    console.log("email : "+ user.email);
    
    try{
        // write code to connect to db
        console.log("waiting saveUser");
        const savedUser = await user.save(); //값 저장
        res.send(savedUser); // 결과값 리턴
        console.log("ended registers")
        // res.send('test'); 
    }catch(err){
        res.status(400).send(err);
    }
});

module.exports = router;
