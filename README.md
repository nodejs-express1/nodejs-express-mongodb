# 프로젝트 초기화
저장소를 복제후 최초 한번 프로젝트를 초기화 한다.
```bash
npm install express --save
```

### 다운로드
http://www.npmjs.org   
expressjs.com

```bash
npm init --yes
npm i express

소스 변경시 핫 디플로이 기능
npm i nodemon
nodemon app.js
```

### 크롬 확장 프로그램 추가 (postman)
postman은 오픈소스로 개발시 테스트를 용이하게 도와주는 브라우저 툴이다.   
[postman 설명](https://meetup.toast.com/posts/107)   
[window download](https://chrome.google.com/webstore/detail/postman/fhbjgbiflinjbdggehcddcbncdddomop)   
[mac download](https://www.getpostman.com/downloads/)



# mongodb 연계
### 1. 서버 생성하기 - app.js
---
app.js 파일을 생성하여 프로젝트 root에 넣고 서버를 생성하는 코드를 작성한다.
#### 1) Node Server 만들기   
express를 선언하고 app에 express() 를 할당해 준다. app.listen(3000) 함수가 실행되면서 로컬에 3000번 포트로 서버가 기동된다.   
```javascript   
// 선언 
const express = require('express');
const app = express();

// 서버 기동 
const port = process.env.PORT || 3000;
app.listen(port, () =>  console.log(`Server started on  port ${port}`));
```





#### 2) 환경변수 프로퍼티 만들기
dotenv module이 이용하여 환경변수를 소스로 부터 분리하여 저장할 수 있다. 환경변수파일은 `.env` 파일로 생성하여 관리한다.   
dotenv 의존서을 추가하한다. package.json에 추가히기 위해 `--save`옵션을 사용한다. 
```bash
npm install dotenv --save
```
루트에 `.env` 파일을 생성하고 값을 정의 한다.
```properties
SERVER_ENV = local
```

javascript에서 선언하고 사용한다.  
```javascript
const dotenv = require("dotenv");
dotenv.config();

//아래와 같이 사용
process.env.SERVER_ENV
```

### 2. mongodb 클러스터 생성하기
---
mongodb를 사용하기 위해서는 1)로컬에 도커를 이용한 설치, 2) 개발 서버에 컨테이너 또는 vm에 설치, 3) 클라우드 서비 이용하기로 구분할 수 있다. 본 예제는 3)번을 이용해서 진행하고자 한다.

#### 1) 클러스터 생성
https://cloud.mongodb.com/ 사이트에 방문에서 free 클러스터로 생성한다. 3~10분 정도 시간이 흐르면 클러스터 생성이 종료되며 아래와 같은 화면을 확인할 수 있다.   

![1.클러스터생성](./images/mongo_01.png)   


#### 2) 계정 추가하기
Database Access 메뉴를 클릭한 후 `add new user` 버튼을 클릭하면 계정 추가 팝업창이 뜬다.   

![2.계정 추가](./images/mongo_02.png)    


계정과 비밀번호, 그리고 권한을 설정한 후 계정 추가 버튼을 클릭한다. 생성된 계정은 목록에서 확인할 수 있다.   

![Prunus](./images/mongo_03.png)   


#### 3) Whitelist 추가하기
로컬 개발 환경에서 클러스터에 접속하기 위해서는 whitelist IP를 추가해 주어야 한다.   
Network Access 메뉴를 클릭하여 추가 화면으로 이동한다. `ADD IP ADDRESS` 버튼을 클릭하여 추가 창을 연다.  

![Prunus](./images/mongo_04.png)   


`ADD CURRENT IP ADDRESS`버튼을 클릭하면 자동으로 Whitelist Entry정보가 생성된다. `Confirm` 버튼을 클릭하여 정보를 추가한다.   

![Prunus](./images/mongo_05.png)   

목록으로 이동하면 추가 작업이 진행되는 모습을 확인할 수 있다. 추가 작업이 완료되면 Status가 `Active`상태로 변경된다.  

![Prunus](./images/mongo_06.png)   


### 3. 계정 등록 API 작성 
---
#### 1) mongoose 모듈을 이용
package intall 명령을 이용하여 mongoose module을 추가해 준다.
```bash
npm install --save mongoose
```
`app.js`파일에 모듈을 import후 connection code를 작성한다.
```javascript
// import module
const mongoose = require('mongoose');

// DB Connecting
mongoose.connect(
    process.env.MONGODB_CON, // 환경변수 정보 
    {
        useNewUrlParser: true ,
        useUnifiedTopology: true 
    },
    () => {
        console.log("DB connecting ...");
    }
);
```

이때 환경변수 정보를 추가해 주어야 하는데 cloud.mongodb.com 에서 접속 정보를 확인한다. 
왼쪽 메뉴에 Clusters를 클릭하면 현재 생성되어 운영중인 클러스터 정보를 확인할 수 있다. 클러스터에서 `CONNECT`버튼을 클릭하여 접속 정보창을 연다.   

![Prunus](./images/mongo_11.png)   

nodejs에서 사용할 접속정보이므로 Connect Your Application을 선택한다.    

![Prunus](./images/mongo_12.png)   

Connection String Only 항목에서 `Copy`버튼을 클릭하여 클립보드에 접속정보 문자열을 복사한다.   

![Prunus](./images/mongo_13.png)   

`.env`파일을 열어 복사한 문자열을 붙여넣기 한다. 
이때 만든 계정이 `test`이고 비밀번호가  `1234`이면 다음과 같이 환경변수를 추가해 주면 된다.
```properties
SERVER_ENV = local
MONGODB_CON = mongodb+srv://test:1234@cluster0-7ljbc.mongodb.net/test?retryWrites=true&w=majority
```

#### 2) 계정 등록 코드 작성
몽고디비 연계를 위한 간단한 코드를 작성하기 위해서는 다음의 파일이 필요하다.
```
 ~/model/user.js    // 몽고디비 스키마 모듈
 ~/routes/auth.js   // api 구현
 ~/app.js           // 서버 실행 파일
 ~/.env             // 환경변수 파일
 
```
##### (1) user.js
관계형디비의 테이블과 같은 개념이 몽고디비에는 존재하지 않는다. 따라서 몽고디비에 데이터를 넣는 형식이 자유롭다. 데이터 형식과 유효성에 대한 보장이 없기 때문에 이를 보완하기 위해 나온 개념이 스키마이다. 스키마는 관계형디비의 테이블과 어느정도 비슷한 역할을 하고 인덱스 까지 생성할 수 있다.   
보통은 한파일에 하나의 스키마를 생성 하지만 스키마가 커지면 여러 파일로 분산해도 된다. 
스키마의 기능은 포맷정의, 인덱스,java의 getter 메소드와 유사한 virtual 등 이 있다. 이 밖에도 다양한 기능이 있는데 [mongoose](https://mongoosejs.com/docs/guide.html)에서 확인이 가능 하다.
```javascript
const mongoose = require("mongoose");

const userSchemea  = new mongoose.Schema({
    name : {
        type: String,
        required: true,
        min: 6, 
        max: 255
    },
    email : {
        type : String, 
        required : true, 
        min : 6,
        max : 255
    },
    password : {
        type : String, 
        required : true,
        min : 6,
        max : 1024
    },
    date : {
        type : Date,
        default : Date.now
    }
});

module.exports = mongoose.model('User', userSchemea);
```
파일은 ```~/model``` 하위 폴더에 작성한다.  

##### (2) auth.js
계정을 생성하기 위한 api를 작성한다. 신규 계정 등록은 위에서 정의한 스키마를 이용하여 몽고디비에 저장하도록 한다.
```javascript
const router  = require("express").Router();
const User = require('../model/user'); // 위치에 주의 한다. 

// api 추가 
// TODO 
module.exports = router;
```
```javascript
/**
 * 계정 등록하기 API
 */
router.post('/register', async (req, res) => {
    console.log("start register");
    const user = new User({ // 스키마에 값 바인딩 
        name : req.body.name,
        email : req.body.email,
        password : req.body.password
    });
    console.log("name : "+ user.name);
    console.log("email : "+ user.email);
    
    try{
        // write code to connect to db
        console.log("waiting saveUser");
        const savedUser = await user.save(); //값 저장
        res.send(savedUser); // 결과값 리턴
        console.log("ended registers")
        // res.send('test'); 
    }catch(err){
        res.status(400).send(err);
    }
});

```
router 객체를 가져와서 ```/register```  api를 추가한 뒤 반환하는 코드이다.
##### (3) app.js
router에 등록한 api를 사용하기 위해서는 app.js 파일에 모듈을 import를 해 주어야 한다.
```javascript
// import module
const authRouter = require("./routes/auth"); // add a auth
```
import된 authRouter를 app.use에 사용한다고 할당해 준다. 이때 prefix를 정할 수 았다. router에 등록된 api ```/registr```는 prefix와 조합하여 사용하게 된다. 
```javascript
// Router middlewares
app.use("/api/user", authRouter);  // prefix, /api/user/***  <- router에 정의된 resource 주소를 subfix로 함 
```
따라서 외부에서 api를 호출할 때의 경로는 ```/api/user/register```가 된다.
확인하기
### 4. 처리결과 확인하기
등록 기능을 확인하기 위해 아래 명령을 이용하여 nodejs 서버를 시작한다. 
```bash
npm start 
```
아래 콘솔 로그가 출력되면서 서버가 정상적으로 실행된 것을 확인한다.
```console
> test@1.0.0 start /Users/test/workspace-node/nodejs-express-mongodb
> nodemon app.js

[nodemon] 2.0.2
[nodemon] to restart at any time, enter `rs`
[nodemon] watching dir(s): *.*
[nodemon] watching extensions: js,mjs,json
[nodemon] starting `node app.js`
Server started on  port 3000
```

#### 1) postman을 이용하여 등록요청 처리
postman을 실행하여 URL을 ```http://localhost:3000/api/user/register```로 입력하고 메소드 타입은 ```POST```를 선택한다. 입력값을 아래 그림과 같이 작성하고 ```send```버튼을 클릭하면 디비에 등록완료 후 그 결과값이 회신됨을 확인할 수 있다.   

![Prunus](./images/mongo_21.png)   


#### 2) 몽고디비에서 확인하기
등록된 정보를 몽고디비에서 확인한다. cloud.mongodb.com 에 로그인 하여 내가 생성한 클러스터를 클릭한다. 클러스터에서 ```collections``` 버튼을 클릭한다.   

![Prunus](./images/mongo_22.png)  

내가 추가한 계정 ```test```의 ```user```스키마에 데이터가 등록되었음을 확인할 수 있다.   

![Prunus](./images/mongo_23.png)
